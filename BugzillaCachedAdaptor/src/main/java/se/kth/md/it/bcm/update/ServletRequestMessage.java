package se.kth.md.it.bcm.update;

import javax.servlet.http.HttpServletRequest;
import org.eclipse.lyo.tools.store.update.OSLCMessage;

/**
 * ServletRequestMessage is .
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public class ServletRequestMessage extends OSLCMessage {

    private final HttpServletRequest httpServletRequest;

    public ServletRequestMessage(final HttpServletRequest httpServletRequest, final String serviceProviderId) {
        super(serviceProviderId);
        this.httpServletRequest = httpServletRequest;
    }

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }
}
