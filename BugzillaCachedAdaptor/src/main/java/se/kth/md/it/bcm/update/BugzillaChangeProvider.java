package se.kth.md.it.bcm.update;

import com.j2bugzilla.base.BugzillaConnector;
import com.j2bugzilla.base.ConnectionException;
import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.eclipse.lyo.oslc4j.bugzilla.Credentials;
import org.eclipse.lyo.oslc4j.bugzilla.exception.UnauthorizedException;
import org.eclipse.lyo.oslc4j.bugzilla.trs.ChangeBugzillaHistories;
import org.eclipse.lyo.oslc4j.bugzilla.trs.HistoryData;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.update.OSLCMessage;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeKind;
import org.eclipse.lyo.tools.store.update.change.ChangeProvider;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.it.bcm.BugzillaCachedAdaptorManager;
import se.kth.md.it.bcm.resources.BugzillaChangeRequest;
import se.kth.md.it.bcm.servlet.CredentialsFilter;

/**
 * BugzillaChangeProvider is responsible for fetching new changes from Bugzilla (usually
 * Landfill) and returning the corresponding {@link Change<ServletRequestMessage>} objects.
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public class BugzillaChangeProvider implements ChangeProvider<OSLCMessage> {
    private final static Logger log = LoggerFactory.getLogger(BugzillaChangeProvider.class);
    private static final ZonedDateTime DEFAULT_DATE = ZonedDateTime.of(1971, 1, 1, 1, 0, 0, 0,
        ZoneId.of("UTC"));
    private final BugzillaConnector bugzillaConnector;

    public BugzillaChangeProvider(final String username, final String password)
        throws ConnectionException, UnauthorizedException {
        bugzillaConnector = CredentialsFilter.getBugzillaConnector(
            new Credentials(username, password));
    }

    @Override
    public Collection<Change<OSLCMessage>> getChangesSince(
        final Optional<ZonedDateTime> timestampOptional,
        final Optional<OSLCMessage> messageOptional) {
        ZonedDateTime since = timestampOptional.orElse(DEFAULT_DATE);
        Date dateSince = Date.from(since.toInstant());
        log.info("Fetching changes since {}", dateSince);
        List<Change<OSLCMessage>> changes = new ArrayList<>();
//        ChangeBugzillaHistories.buildBaseResourcesAndChangeLogs(bugzillaConnector);
//        HistoryData[] changeHistory = ChangeBugzillaHistories.getChangeHistory();
        HistoryData[] changeHistory = ChangeBugzillaHistories.getHistory(bugzillaConnector,
            dateSince);
        log.debug("Fetched {} changes", changeHistory.length);

        HashMap<URI, BugzillaChangeRequest> resources = new HashMap<>();
        for (HistoryData historyData : changeHistory) {
            if (historyData.getTimestamp().compareTo(dateSince) > 0) {
                AbstractResource resource = fetchOnce(resources, historyData);
                final HistoryResource historyResource = BugzillaChangeProvider.historyResourceFrom(
                    historyData);
                final OSLCMessage resourceMessage = new OSLCMessage(String.valueOf(historyData
                    .getProductId()));
                Change<OSLCMessage> change = new Change<>(resource, historyResource,
                    resourceMessage);
                changes.add(change);
            }
        }
        return changes;
    }

    /**
     * Wraps adaptor's {@link HistoryData} object with {@link HistoryResource} from Lyo Store.
     * The latter can also be marshaled into triples.
     * @param historyData Bugzilla {@link HistoryData} object for Create/Modify event.
     * @return {@link HistoryResource} wrapper object (OSLC compatible)
     */
    private static HistoryResource historyResourceFrom(final HistoryData historyData) {
        // TODO think of deletion events, HistoryData doesn't support it
        ChangeKind changeKind = ChangeKind.fromString(historyData.getType());
        return new HistoryResource(changeKind, historyData.getTimestamp(), historyData.getUri());
    }

    /**
     * Fetch a {@link BugzillaChangeRequest} from Bugzilla once per call.
     */
    private AbstractResource fetchOnce(final HashMap<URI, BugzillaChangeRequest> resources, final
    HistoryData historyData) {
        if (!resources.containsKey(historyData.getUri())) {
            BugzillaChangeRequest aResource = BugzillaCachedAdaptorManager
                .getBugzillaChangeRequestFromServer(
                bugzillaConnector, String.valueOf(historyData.getProductId()),
                String.valueOf(historyData.getBugId()));
            if (aResource != null) {
                resources.put(historyData.getUri(), aResource);
                return aResource;
            }
        }
        return null;
    }
}
