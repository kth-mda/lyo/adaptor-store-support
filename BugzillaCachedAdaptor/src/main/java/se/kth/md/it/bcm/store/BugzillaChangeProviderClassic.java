package se.kth.md.it.bcm.store;

import java.net.URI;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.eclipse.lyo.oslc4j.bugzilla.trs.HistoryData;
import org.eclipse.lyo.oslc4j.bugzilla.trs.ChangeBugzillaHistories;
import se.kth.md.it.bcm.BugzillaCachedAdaptorManager;
import se.kth.md.it.bcm.resources.BugzillaChangeRequest;

@Deprecated
public class BugzillaChangeProviderClassic implements ChangeProvider {

	@Override
	public Collection<ChangeStatus> getChangesSince(HttpServletRequest httpServletRequest, Date latestUpdate) {
		List<ChangeStatus> changes = new ArrayList<ChangeStatus>();
		try {
			ChangeBugzillaHistories.buildBaseResourcesAndChangeLogs(httpServletRequest);
			HistoryData[] changeHistory = ChangeBugzillaHistories.getChangeHistory();
	
			HashMap<URI, BugzillaChangeRequest> resources = new HashMap<URI, BugzillaChangeRequest>();
			for (HistoryData historyData : changeHistory) {
				if (historyData.getTimestamp().compareTo(latestUpdate) > 0)
				{
		    		if (!resources.containsKey(historyData.getUri())) {
		    			BugzillaChangeRequest aResource = BugzillaCachedAdaptorManager.getBugzillaChangeRequestFromServer(httpServletRequest, String.valueOf(historyData.getProductId()), String.valueOf(historyData.getBugId()));
				        if (aResource != null) {
				    		resources.put(historyData.getUri(), aResource);
				        }
		    		}
		    		changes.add(new ChangeStatus(historyData.getUri(), historyData.getTimestamp(), historyData.getType(), resources.get(historyData.getUri())));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(e,Status.INTERNAL_SERVER_ERROR);
		}
		return changes;
	}
}
