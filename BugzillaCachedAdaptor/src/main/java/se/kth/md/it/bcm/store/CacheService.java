/*******************************************************************************
 * Copyright (c) 2012 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *
 *     Russell Boykin       - initial API and implementation
 *     Alberto Giammaria    - initial API and implementation
 *     Chris Peters         - initial API and implementation
 *     Gianluca Bernardini  - initial API and implementation
 *     Michael Fiedler      - implementation for Bugzilla adapter
 *     Jad El-khoury        - initial implementation of code generator (https://bugs.eclipse.org/bugs/show_bug.cgi?id=422448)
 *
 * This file is generated by org.eclipse.lyo.oslc4j.codegenerator
 *******************************************************************************/

package se.kth.md.it.bcm.store;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Deprecated
@Path("cache")
public class CacheService
{
	@Context private HttpServletRequest httpServletRequest;
	@Context private HttpServletResponse httpServletResponse;
	@Context private UriInfo uriInfo;
   

    @GET
    public void cache()
    {
		try {
	        RequestDispatcher rd = httpServletRequest.getRequestDispatcher("/se/kth/md/it/bcm/cachemanager.jsp");
			rd.forward(httpServletRequest, httpServletResponse);
		} catch (Exception e) {				
			e.printStackTrace();
			throw new WebApplicationException(e);
		} 
    }

    @GET
    @Path("update")
    public void updateCacheManager()
    {
		try {
			StoreManager.update(httpServletRequest);
	        RequestDispatcher rd = httpServletRequest.getRequestDispatcher("/se/kth/md/it/bcm/cachemanagerupdate.jsp");
			rd.forward(httpServletRequest, httpServletResponse);
		} catch (Exception e) {				
			e.printStackTrace();
			throw new WebApplicationException(e);
		} 
    }

}

