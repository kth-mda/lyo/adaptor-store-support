package se.kth.md.it.bcm.store;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.kth.md.it.bcm.servlet.ServletListener;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import java.util.Base64;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.Store;

@Deprecated
class StoreManager {
	private static final Logger log = LoggerFactory.getLogger(StoreManager.class);
	private static Store store;
	private static ChangeProvider changeProvider;
	private static boolean storeEnabled;
	private static Date latestStoreUpdate;
	private static int storeUpdateFrequency;
	
	private static String bugzillaUsername;
	private static String bugzillaPassword;

	private StoreManager() {
		super();
	}

	public static Store getStore() {
		return store;
	}

	public static ChangeProvider getChangeProvider() {
		return changeProvider;
	}

	public static void setChangeProvider(ChangeProvider changeProvider) {
		StoreManager.changeProvider = changeProvider;
	}

	public static void update(HttpServletRequest httpServletRequest) {
		if (!storeEnabled) {
			throw new IllegalStateException("StoreManager is not enabled");
		}
		if (changeProvider == null) {
			throw new IllegalStateException("setChangeProvider() must be called before invoking the update() method");
		}

		System.out.println("updating based on earlier latestStoreUpdate of:" + StoreManager.latestStoreUpdate.toString());
		Collection<ChangeStatus> changes = changeProvider.getChangesSince(httpServletRequest, latestStoreUpdate);
		Set<ChangeStatus> changesAsSet = new HashSet<ChangeStatus>(changes);

		Date latestChange = latestStoreUpdate;
		AbstractResource aResource;
		String cacheKey;
		for (ChangeStatus aChange : changesAsSet) {
			if (aChange.getOccured().compareTo(latestChange) > 0)
			{
				latestChange = aChange.getOccured();
			}
			aResource = aChange.getResource();
			cacheKey = aResource.getAbout().toString();
			try {
				if (store.containsKey(cacheKey)) {
					store.remove(cacheKey);
				}
				store.putResources(cacheKey, aResource);
			} catch (Exception e) {
				System.out.println("PrintStackTrace in method updateCacheManager for: " + cacheKey);
				e.printStackTrace();
			}
		}
		latestStoreUpdate = latestChange;
	}

	public static void scheduleStoreUpdates() {
		if (!storeEnabled) {
			throw new IllegalStateException("StoreManager is not enabled");
		}
		
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		Runnable task = new Runnable() {
			public void run() {
				try {
				    System.out.println("Scheduling: " + System.nanoTime());
				    
			        String basePath = ServletListener.getServicesBase();
			        UriBuilder builder = UriBuilder.fromUri(basePath);
			        builder.path("cache/update");
				    HttpClient client = new DefaultHttpClient();
				    HttpGet request = new HttpGet(builder.build().toString());
				    String encoding = Base64.getEncoder().encodeToString((bugzillaUsername + ":" + bugzillaPassword).getBytes());
				    request.setHeader("Authorization", "Basic " + encoding);
				    HttpResponse response = client.execute(request);
				    BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
				    String line = "";
				    while ((line = rd.readLine()) != null) {
				      System.out.println(line);
				    }
				} catch (Exception e) {
					log.error(String.valueOf(e));
				}
			}
		};
		executor.scheduleAtFixedRate(task, 0, storeUpdateFrequency, TimeUnit.MINUTES);
	}
}
