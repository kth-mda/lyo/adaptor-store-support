package se.kth.md.it.bcm.store;

import java.util.Collection;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

@Deprecated
public interface ChangeProvider {

	Collection<ChangeStatus> getChangesSince(HttpServletRequest httpServletRequest, Date latestUpdate);

}
