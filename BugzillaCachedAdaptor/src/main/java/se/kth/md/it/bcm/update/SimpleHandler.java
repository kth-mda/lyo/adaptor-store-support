package se.kth.md.it.bcm.update;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang.NullArgumentException;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.OSLCMessage;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SimpleHandler is .
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public class SimpleHandler implements Handler<OSLCMessage> {
    private final static Logger log = LoggerFactory.getLogger(SimpleHandler.class);

    @Override
    public void handle(final Store store, final Collection<Change<OSLCMessage>> changes,
        final Optional<OSLCMessage> messageOptional) {
        OSLCMessage message = messageOptional.orElseThrow(() -> new NullArgumentException(
            "Handler requires Service Provider ID in order to insert the resources into the " +
                "triplestore"));
        String serviceProviderId = message.getServiceProviderId();
        try {
            List<AbstractResource> resourceList = ChangeHelper.mapFn(changes,
                Change::getResource).stream().filter(Objects::nonNull).collect(Collectors.toList());
            log.info("Storing {} resources under '{}' service provider", resourceList.size(),
                serviceProviderId);
            if (store.containsKey(serviceProviderId)) {
                store.appendResources(serviceProviderId, resourceList);
            } else {
                store.putResources(serviceProviderId, resourceList);
            }
        } catch (StoreAccessException e) {
            log.error("Error while inserting resources into the triplestore", e);
        }
    }
}
