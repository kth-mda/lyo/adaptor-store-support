Bugzilla sample adaptor with Store support
===============================================

This adaptor is based on the plain Bugzilla sample adaptor but adds support for OSLC Resource persistence (via OSLC POJOs) using Lyo Store library.

## First steps

Create a file under the following location:

* `%userprofile%\test\resources\bugz.properties` for Windows
* `~/test/resources/bugz.properties` for Linux and macOS

with the following contents:

    bugzilla_uri = https://landfill.bugzilla.org/bugzilla-4.4-branch/
    admin = landfill_account_email@localhost
    storeEnabled = true
    runSparql = false
    # typically the same as admin email
    bugzillaUsername = landfill_account_email@localhost
    bugzillaPassword = ???????????
